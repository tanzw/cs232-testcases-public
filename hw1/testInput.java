// The aspect that makes this test case interesting is that it uses an method that has been 
// overridden and it also uses a method that has been defined in the parent class but not the child.

class Example {
	public static void main(String[] a){
		Animal a = new Animal();
		System.out.println(a.makeSound());
		a.wallk(10, 5);
		Dog d = new Dog();
		System.out.println(d.makeSound());
	}

}

class Animal {
	public String makeSound(){
		return "default sound";
	}
	public void walk(int spaces, int moves){
		if(moves >= spaces){
			return 0;
		}
		else{
			return spaces - moves;
		}
	}


}

class Dog extends Animal {
	public String makeSound(){
		return "Ruff Ruff";
	}
}
